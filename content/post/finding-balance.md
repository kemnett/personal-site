+++
title = "Finding balance"

date = 2016-04-20T00:00:00
lastmod = 2018-09-01T00:00:00
draft = false

tags = ["personal"]
summary = "Balancing work, family, personal and professional relationships, health, etc."


+++

A long time ago, before I understood the metaphor, I had a boss tell me that I needed to "go spin that plate". He was referring to a responsibility of mine that appeared to need some attention. I picked up on what he was getting at but had no idea what the metaphor actually meant (I was young, cut me some slack). He was, of course, referring to the practice of spinning plates on poles like you might have seen done in a street act at some point. When one plate starts slowing down, you give it a quick spin to keep it going. The performer has to constantly go back and forth through the plates and give them each a spin until momentum is no longer on their side and plates start falling.

![balance](/img/balance-scale-tilted-right_4460x4460.jpg)

It's easy to see why this is such a great metaphor for what most call "work-life balance". I, like most of you, have a personal goal to always be a little bit better than I was yesterday. (cliche I know) I'm not saying that I'm not happy with where I am in life, that couldn't be farther from the truth. However, I don't like the idea that I may wake up one day, and some "thing" about me is worse than it was yesterday. I want to be a lifelong learner, someone who maintains a healthy lifestyle, maintains healthy personal and professional relationships, finds ways to be a better dad and husband, etc. Without giving each of those plates a spin on a regular basis, the plate can lose momentum rather quickly.

So how do we make sure that we're constantly moving forward, and not backward? Like anything in life, it takes effort. I'm frequently telling my kids that they can't get better at anything without practice and repetition. Practice and repetition lead to mastery. You have to build in time management mechanisms and keep up with them in order to ensure that you keep things on track. Something as simple as keeping an online calendar might seem completely obvious to most, but before smartphones, I didn't keep a calendar at all. Now, I couldn't imagine life without it. Ensuring that we have all of our workouts, kids activities, work, and social events all in sync with a shared calendar is probably the single most useful time management technique that my wife and I have.

I keep a lot of lists and schedules. I'm sure nearly everyone does, at least at work. You should take time to prioritize your personal life, just like you do at work. If you're a successful individual at all, I guarantee you not only know what work needs to be done but what the prioritization of that work is. I can even go a step further and venture a guess that you carefully plan those things out and are rarely surprised by outcomes. That's just the way successful people work. Some people do a great job of implementing these strategies at work, and then for one reason or another, they can't see how useful they would be in their personal life.

Take your fitness for example. I don't know anyone that maintains a high level of fitness without a plan or routine of some sort. Planning and programming are two of the most important aspects of fitness no matter what your goals are. Maybe it's to just have a well-balanced level of fitness, maybe it's to be a powerlifter, or maybe you want to break a certain time in a marathon. The path to all of those things starts with a plan and sticking to the plan.

Not everything needs to be written down, but you should understand that you need to take the extra time for the things that matter. That might be just understanding that, yes, you do have time to read to your kids for thirty minutes before bed even though you have a work project due in the morning. Taking that a step further, you're also going to finish that project, and wake up thirty minutes earlier and get your run out of the way in the morning. You'll be better for it. Time keeps on going, you don't want to be the person that has to make excuses for why you didn't give that plate a spin.  
